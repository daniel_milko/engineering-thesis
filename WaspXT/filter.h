/*
 * filter.h
 *
 * Created: 23.11.2020 12:08:23
 *  Author: danie
 */ 


#ifndef FILTER_H_
#define FILTER_H_

typedef struct  {
	
		int kbTrkValue;
		int envValue;
		int cutoffValue;
		int resoValue;
		
		int lpValue;
		int lpPlusNtValue;
		int lp2Value;
		int dblDotNtValue;
		int bpValue;
		int hpValue;
		
		int kbTrkMidiControl;
		int envMidiControl;
		int cutoffMidiControl;
		int resoMidiControl;
		int buttonsMidiControl;
		
	} FilterSettings;

FilterSettings filterSettings = {
			.kbTrkValue = -1,
			.envValue = -1,
			.cutoffValue = -1,
			.resoValue = -1,
			
			.lpValue = 0, 
			.lpPlusNtValue = 25,
			.lp2Value = 50, 
			.dblDotNtValue = 75,
			.bpValue = 100,
			.hpValue = 127,
			
			.kbTrkMidiControl = 1,
			.envMidiControl = 2,
		    .cutoffMidiControl = 3,
			.resoMidiControl = 4,
			.buttonsMidiControl = 5,
	};


#endif /* FILTER_H_ */