/*
 * osc3.h
 *
 * Created: 26.11.2020 16:03:33
 *  Author: danie
 */ 


#ifndef OSC3_H_
#define OSC3_H_

typedef struct  {
	
	int amtValue;
	
	int triangleValue;
	int squareValue;
	
	int amtMidiControl;
	int buttonWaveMidiControl;
	
} Osc3Settings;

Osc3Settings osc3Settings = {
	.amtValue = -1,
	
	.triangleValue = 0,
	.squareValue = 127,

	.amtMidiControl = 51,
	.buttonWaveMidiControl = 52,
};



#endif /* OSC3_H_ */