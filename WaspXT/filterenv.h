/*
 * filterenv.h
 *
 * Created: 26.11.2020 15:39:04
 *  Author: danie
 */ 


#ifndef FILTERENV_H_
#define FILTERENV_H_


typedef struct  {
	
	int atkValue;
	int decValue;
	int susValue;
	int relValue;

	int linkValue;
	int linkWasPressed;
	
	int atkMidiControl;
	int decMidiControl;
	int susMidiControl;
	int relMidiControl;
	int linkMidiControl;
	
} FilterEnvSettings;

FilterEnvSettings filterEnvSettings = {
	.atkValue = -1,
	.decValue = -1,
	.susValue = -1,
	.relValue = -1,
	
	.linkValue = 0,
	.linkWasPressed = 0,
	
	.atkMidiControl = 40,
	.decMidiControl = 41,
	.susMidiControl = 42,
	.relMidiControl = 43,
	.linkMidiControl = 44,
};


#endif /* FILTERENV_H_ */