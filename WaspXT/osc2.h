/*
 * osc2.h
 *
 * Created: 26.11.2020 15:56:04
 *  Author: danie
 */ 


#ifndef OSC2_H_
#define OSC2_H_


typedef struct  {
	
	int coarseValue;
	int fineValue;

	int triangleValue;
	int squareValue;
	int sinusValue;
	int noiseValue;
	
	int coarseMidiControl;
	int fineMidiControl;
	int buttonWaveMidiControl;
	
} Osc2Settings;

Osc2Settings osc2Settings = {
	.coarseValue = -1,
	.fineValue = -1,

	.triangleValue = 0,
	.squareValue = 50,
	.sinusValue = 100,
	.noiseValue = 127,
	
	.coarseMidiControl = 48,
	.fineMidiControl = 49,
	.buttonWaveMidiControl = 50,
};


#endif /* OSC2_H_ */