/*
 * lfo2.h
 *
 * Created: 26.11.2020 14:21:18
 *  Author: danie
 */ 


#ifndef LFO2_H_
#define LFO2_H_

typedef struct  {
	
	int amountValue;
	int rateValue;
	int delayValue;
	
	int triangleValue;
	int squareValue;
	int sinusValue;
	int noiseValue;
	
	int oscOneValue;
	int oscMixValue;
	int ampValue;
	
	int syncValue;
	int resetValue;
	int syncWasPressed;
	int resetWasPressed;
	
	int amountMidiControl;
	int rateMidiControl;
	int delayMidiControl;
	int buttonWaveMidiControl;
	int buttonSetupsMidiControl;
	int buttonSyncMidiControl;
	int buttonResetMidiControl;
	
} Lfo2Settings;

Lfo2Settings lfo2Settings = {
	.amountValue = -1,
	.rateValue = -1,
	.delayValue = -1,
	
	.triangleValue = 0,
	.squareValue = 50,
	.sinusValue = 100,
	.noiseValue = 127,
	
	.oscOneValue = 0,
	.oscMixValue = 70,
	.ampValue = 127,
	
	.resetValue = 0,
	.resetWasPressed = 0,
	.syncValue = 0,
	.syncWasPressed = 0,
	
	.amountMidiControl = 13,
	.rateMidiControl = 14,
	.delayMidiControl = 15,
	.buttonWaveMidiControl = 16,
	.buttonSetupsMidiControl = 17,
	.buttonSyncMidiControl = 18,
	.buttonResetMidiControl = 19,
};



#endif /* LFO2_H_ */