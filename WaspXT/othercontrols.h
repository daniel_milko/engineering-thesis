#ifndef OTHERCONTROLS_H_
#define OTHERCONTROLS_H_

typedef struct  {
	
	int dualValue;
	int dualWasPressed;

	int analogValue;
	int analogWasPressed;

	int wNoiseValue;
	int wNoiseWasPressed;
	
	int dualMidiControl;
	int analogMidiControl;
	int wNoiseMidiControl;
	
} OtherControlsSettings;

OtherControlsSettings otherControlsSettings = {

	.dualValue = 0,
	.dualWasPressed = 0,

	.analogValue = 0,
	.analogWasPressed = 0,

	.wNoiseValue = 0,
	.wNoiseWasPressed = 0,
	
	.dualMidiControl = 33,
	.analogMidiControl = 34,
	.wNoiseMidiControl = 35,
	
};



#endif /* OTHERCONTROLS_H_ */