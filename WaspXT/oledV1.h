/*
 * oledV1.h
 *
 * Created: 17.12.2020 17:18:24
 *  Author: danie
 */ 


#ifndef OLEDV1_H_
#define OLEDV1_H_


typedef struct  {
	
	uint8_t atkLine;
	uint8_t decLine;
	uint8_t susLine;
	uint8_t relLine;
	
} OledV1Settings;

OledV1Settings oledV1Settings = {
	.atkLine = 0,
	.decLine = 0,
	.susLine = 0,
	.relLine = 0,
};

OledV1Settings oledV2Settings = {
	.atkLine = 0,
	.decLine = 0,
	.susLine = 0,
	.relLine = 0,
};



#endif /* OLEDV1_H_ */