/*
 * output.h
 *
 * Created: 26.11.2020 15:17:12
 *  Author: danie
 */ 


#ifndef OUTPUT_H_
#define OUTPUT_H_

typedef struct  {
	
	int toneValue;
	int amtValue;
	int volValue;

	int driveValue;
	int driveWasPressed;
	
	int toneMidiControl;
	int amtMidiControl;
	int volMidiControl;
	int driveMidiControl;
	
} OutputSettings;

OutputSettings outputSettings = {
	.toneValue = -1,
	.amtValue = -1,
	.volValue = -1,
	
	.driveValue = 0,
	.driveWasPressed = 0,
	
	.toneMidiControl = 29,
	.amtMidiControl = 30,
	.volMidiControl = 31,
	.driveMidiControl = 32,
	
};



#endif /* OUTPUT_H_ */