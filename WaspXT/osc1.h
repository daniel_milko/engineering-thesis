/*
 * osc1.h
 *
 * Created: 26.11.2020 15:42:47
 *  Author: danie
 */ 


#ifndef OSC1_H_
#define OSC1_H_

typedef struct  {
	
	int coarseValue;
	int fineValue;

	int triangleValue;
	int squareValue;
	int sinusValue;
	int noiseValue;
	
	int coarseMidiControl;
	int fineMidiControl;
	int buttonWaveMidiControl;
	
} Osc1Settings;

Osc1Settings osc1Settings = {
	.coarseValue = -1,
	.fineValue = -1,

	.triangleValue = 0,
	.squareValue = 50,
	.sinusValue = 100,
	.noiseValue = 127,
	
	.coarseMidiControl = 45,
	.fineMidiControl = 46,
	.buttonWaveMidiControl = 47,
};

#endif /* OSC1_H_ */