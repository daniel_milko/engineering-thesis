#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <stdlib.h>
#include <stdbool.h>


#include "usbdrv.h"
#include "oddebug.h"
#include "descriptor.inc"

#define MIDI_PORT 0xb1

#include "filter.h"
#include "lfo2.h"
#include "ampenv.h"
#include "modenv.h"
#include "filterenv.h"
#include "lfo1.h"
#include "velocity.h"
#include "osc1.h"
#include "osc2.h"
#include "osc3.h"
#include "oscmix.h"
#include "output.h"
#include "othercontrols.h"
#include "diodes.h"
#include "oledV1.h"
#include "SSD1306.h"

#include "usbdrv.h"
#include "oddebug.h"

#define nameDescrMIDI_LEN 12


void UART_init(unsigned int baud,bool RX,bool TX)
{
	unsigned int ubrr;
	ubrr=(((F_CPU / (baud * 16UL))) - 1);
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	UCSR0C |= 1<<UPM01;
	UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00);
	
	if(RX)
	{
		UCSR0B |= 1<<RXEN0;
	}
	if(TX)
	{
		UCSR0B |= 1<<TXEN0;
	}
}
void USART_send( uint8_t data){
	
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = data;
	while(!(UCSR0A & (1<<UDRE0)));
	
}
bool UART_available()
{
	return (UCSR0A & (1<<RXC0));
}

uint8_t UART_read()
{
	while(!(UCSR0A & (1<<RXC0)));
	return UDR0;
}

static int nameDescrMIDI[nameDescrMIDI_LEN+1] = {
	USB_STRING_DESCRIPTOR_HEADER( nameDescrMIDI_LEN ),
	'W', 'a', 's', 'p', 'X', 'T','-','M','I','D','I'
};


uchar usbFunctionDescriptor(usbRequest_t * rq)
{
	uchar len = 0;
	usbMsgPtr = 0;
	if ( rq->wValue.bytes[1] == USBDESCR_DEVICE ) {
		usbMsgPtr = (uchar *) deviceDescrMIDI;
		len = sizeof( deviceDescrMIDI );
		} else if (rq->wValue.bytes[1] == USBDESCR_CONFIG ) {
		usbMsgPtr = (uchar *) configDescrMIDI;
		len = sizeof( configDescrMIDI );
	} else if ( rq->wValue.bytes[1] == USBDESCR_STRING
	&& rq->wValue.bytes[0] == 2 ) {
		usbMsgPtr = (uchar *) nameDescrMIDI;
		len = sizeof( nameDescrMIDI );
	}
	return len;

}


uchar usbFunctionSetup(uchar data[8])
{
	return 0xff;
}

void usbFunctionWriteOut(uchar * data, uchar len)
{

}


static void hardwareInit(void)
{
	uchar i, j;

	/* activate pull-ups except on USB lines */
	USB_CFG_IOPORT =
	(uchar) ~ ((1 << USB_CFG_DMINUS_BIT) |
	(1 << USB_CFG_DPLUS_BIT));
	/* all pins input except USB (-> USB reset) */

	usbDeviceDisconnect();

	j = 0;
	while (--j) {		/* USB Reset by device only required on Watchdog Reset */
		i = 0;
		while (--i);	/* delay >10ms for USB reset */
	}
	usbDeviceConnect();

	ADMUX |= (1<<REFS0);
	ADCSRA |= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADEN);

}

int adc(uchar channel)
{
	//select ADC channel with safety mask
	ADMUX = ADMUX & 0xf0;
	ADMUX = ADMUX | (channel & 0x0f);
	//single conversion mode
	ADCSRA |= (1<<ADSC);
	// wait until ADC conversion is complete
	while( ADCSRA & (1<<ADSC) );
	return ADC;
}

void adcZeroController(int value, int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 0){
		if (filterSettings.kbTrkValue - value > 7 || filterSettings.kbTrkValue - value < -7) {
			filterSettings.kbTrkValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterSettings.kbTrkMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 1){
		if (filterSettings.envValue - value > 7 || filterSettings.envValue - value < -7) {
			filterSettings.envValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterSettings.envMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 2){
		if (filterSettings.cutoffValue - value > 7 || filterSettings.cutoffValue - value < -7) {
			filterSettings.cutoffValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterSettings.cutoffMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 3){
		if (filterSettings.resoValue - value > 7 || filterSettings.resoValue - value < -7) {
			filterSettings.resoValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterSettings.resoMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 4){
		if (lfo1Settings.amountValue - value > 7 || lfo1Settings.amountValue - value < -7) {
			lfo1Settings.amountValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.amountMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 5){
		if (lfo1Settings.rateValue - value > 7 || lfo1Settings.rateValue - value < -7) {
			lfo1Settings.rateValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.rateMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 6){
		if (lfo1Settings.delayValue - value > 7 || lfo1Settings.delayValue - value < -7) {
			lfo1Settings.delayValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.delayMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 7){
		if (lfo2Settings.amountValue - value > 7 || lfo2Settings.amountValue - value < -7) {
			lfo2Settings.amountValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.amountMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
	}
	
}

void adcOneController(int value, int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 8){
		if (lfo2Settings.rateValue - value > 7 || lfo2Settings.rateValue - value < -7) {
			lfo2Settings.rateValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.rateMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 9){
		if (lfo2Settings.delayValue - value > 7 || lfo2Settings.delayValue - value < -7) {
			lfo2Settings.delayValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.delayMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 10){
		if (modEnvSettings.atkValue - value > 7 || modEnvSettings.atkValue - value < -7) {
			modEnvSettings.atkValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & modEnvSettings.atkMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 11){
		if (modEnvSettings.decValue - value > 7 || modEnvSettings.decValue - value < -7) {
			modEnvSettings.decValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & modEnvSettings.decMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 12){
		if (modEnvSettings.plusAmtMinusValue - value > 7 || modEnvSettings.plusAmtMinusValue - value < -7) {
			modEnvSettings.plusAmtMinusValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & modEnvSettings.plusAmtMinusMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 13){
		if (velocitySettings.filterValue - value > 7 || velocitySettings.filterValue - value < -7) {
			velocitySettings.filterValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & velocitySettings.filterMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 14){
		if (velocitySettings.ampValue - value > 7 || velocitySettings.ampValue - value < -7) {
			velocitySettings.ampValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & velocitySettings.ampMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 15){
		if (outputSettings.toneValue - value > 7 || outputSettings.toneValue - value < -7) {
			outputSettings.toneValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & outputSettings.toneMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
	}
	
}

void adcTwoController(int value, int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 16){
		if (outputSettings.amtValue - value > 7 || outputSettings.amtValue - value < -7) {
			outputSettings.amtValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & outputSettings.amtMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 17){
		if (outputSettings.volValue - value > 7 || outputSettings.volValue - value < -7) {
			outputSettings.volValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & outputSettings.volMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 18){
		if (ampEnvSettings.atkValue - value > 7 || ampEnvSettings.atkValue - value < -7) {
			ampEnvSettings.atkValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & ampEnvSettings.atkMidiControl;
			midiMsg[3] = value >> 3;
			oledV1Settings.atkLine = midiMsg[3] / 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 19){
		if (ampEnvSettings.decValue - value > 7 || ampEnvSettings.decValue - value < -7) {
			ampEnvSettings.decValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & ampEnvSettings.decMidiControl;
			midiMsg[3] = value >> 3;
			oledV1Settings.decLine = midiMsg[3] / 3;			
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 20){
		if (ampEnvSettings.susValue - value > 7 || ampEnvSettings.susValue - value < -7) {
			ampEnvSettings.susValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & ampEnvSettings.susMidiControl;
			midiMsg[3] = value >> 3;
			oledV1Settings.susLine = midiMsg[3] / 2;	
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 21){
		if (ampEnvSettings.relValue - value > 7 || ampEnvSettings.relValue - value < -7) {
			ampEnvSettings.relValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & ampEnvSettings.relMidiControl;
			midiMsg[3] = value >> 3;
			oledV1Settings.relLine = midiMsg[3] / 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 22){
		if (filterEnvSettings.atkValue - value > 7 || filterEnvSettings.atkValue - value < -7) {
			filterEnvSettings.atkValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterEnvSettings.atkMidiControl;
			midiMsg[3] = value >> 3;
			oledV2Settings.atkLine = midiMsg[3] / 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 23){
		if (filterEnvSettings.decValue - value > 7 || filterEnvSettings.decValue - value < -7) {
			filterEnvSettings.decValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterEnvSettings.decMidiControl;
			midiMsg[3] = value >> 3;
			oledV2Settings.decLine = midiMsg[3] / 3;
			usbSetInterrupt(midiMsg, 4);
		}
	}
	
}

void adcThreeController(int value, int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 24){
		if (filterEnvSettings.susValue - value > 7 || filterEnvSettings.susValue - value < -7) {
			filterEnvSettings.susValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterEnvSettings.susMidiControl;
			midiMsg[3] = value >> 3;
			oledV2Settings.susLine = midiMsg[3] / 2;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 25){
		if (filterEnvSettings.relValue - value > 7 || filterEnvSettings.relValue - value < -7) {
			filterEnvSettings.relValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & filterEnvSettings.relMidiControl;
			midiMsg[3] = value >> 3;
			oledV2Settings.relLine = midiMsg[3] / 2;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 26){
		if (osc1Settings.coarseValue - value > 7 || osc1Settings.coarseValue - value < -7) {
			osc1Settings.coarseValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc1Settings.coarseMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 27){
		if (osc1Settings.fineValue - value > 7 || osc1Settings.fineValue - value < -7) {
			osc1Settings.fineValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc1Settings.fineMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 28){
		if (osc2Settings.coarseValue - value > 7 || osc2Settings.coarseValue - value < -7) {
			osc2Settings.coarseValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc2Settings.coarseMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 29){
		if (osc2Settings.fineValue - value > 7 || osc2Settings.fineValue - value < -7) {
			osc2Settings.fineValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc2Settings.fineMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 30){
		if (osc3Settings.amtValue - value > 7 || osc3Settings.amtValue - value < -7) {
			osc3Settings.amtValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc3Settings.amtMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 31){
		if (oscMixSettings.oscmixValue - value > 7 || oscMixSettings.oscmixValue - value < -7) {
			oscMixSettings.oscmixValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & oscMixSettings.oscmixMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
	}
	
}

void adcFourController(int value, int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 33){
		if (oscMixSettings.fmValue - value > 7 || oscMixSettings.fmValue - value < -7) {
			oscMixSettings.fmValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & oscMixSettings.fmMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 32){
		if (oscMixSettings.pwValue - value > 7 || oscMixSettings.pwValue - value < -7) {
			oscMixSettings.pwValue = value;
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & oscMixSettings.pwMidiControl;
			midiMsg[3] = value >> 3;
			usbSetInterrupt(midiMsg, 4);
		}
	}
}

void buttonsZeroController(int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 40){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & filterSettings.buttonsMidiControl;
				midiMsg[3] = filterSettings.lpValue;
				diodesSettings.ledMuxZero &= 3;
				diodesSettings.ledMuxZero |= 128; 
				usbSetInterrupt(midiMsg, 4);
			} 
		} else if(mux_channel == 41){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & filterSettings.buttonsMidiControl;
				midiMsg[3] = filterSettings.lpPlusNtValue;
				diodesSettings.ledMuxZero &= 3;
				diodesSettings.ledMuxZero |= 64; 
				usbSetInterrupt(midiMsg, 4);
			}
		} else if(mux_channel == 42){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & filterSettings.buttonsMidiControl;
				midiMsg[3] = filterSettings.lp2Value;
				diodesSettings.ledMuxZero &= 3;
				diodesSettings.ledMuxZero |= 32;
				usbSetInterrupt(midiMsg, 4);
			}
		} else if(mux_channel == 43){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & filterSettings.buttonsMidiControl;
				midiMsg[3] = filterSettings.dblDotNtValue;
				diodesSettings.ledMuxZero &= 3;
				diodesSettings.ledMuxZero |= 16;
				usbSetInterrupt(midiMsg, 4);
			}
		} else if(mux_channel == 44){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & filterSettings.buttonsMidiControl;
				midiMsg[3] = filterSettings.bpValue;
				diodesSettings.ledMuxZero &= 3;
				diodesSettings.ledMuxZero |= 8;
				usbSetInterrupt(midiMsg, 4);
			}
		} else if(mux_channel == 45){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & filterSettings.buttonsMidiControl;
				midiMsg[3] = filterSettings.hpValue;
				diodesSettings.ledMuxZero &= 3;
				diodesSettings.ledMuxZero |= 4;
				usbSetInterrupt(midiMsg, 4);
			}
		} else if(mux_channel == 46){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo1Settings.buttonWaveMidiControl;
				midiMsg[3] = lfo1Settings.triangleValue;
				diodesSettings.ledMuxZero &= 252;
				diodesSettings.ledMuxOne &= 63; 
				diodesSettings.ledMuxZero |= 2;
				usbSetInterrupt(midiMsg, 4);
			}
		} else if(mux_channel == 47){
			if( !(PIND & (1<<PORTD4))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo1Settings.buttonWaveMidiControl;
				midiMsg[3] = lfo1Settings.squareValue;
				diodesSettings.ledMuxZero &= 252;
				diodesSettings.ledMuxOne &= 63; 
				diodesSettings.ledMuxZero |= 1;
				usbSetInterrupt(midiMsg, 4);
			}
		}

}

void buttonsOneController(int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 48){
		if( !(PIND & (1<<PORTD5))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.buttonWaveMidiControl;
			midiMsg[3] = lfo1Settings.sinusValue;
			diodesSettings.ledMuxZero &= 252;
			diodesSettings.ledMuxOne &= 63; 
			diodesSettings.ledMuxOne |= 128;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 49){
		if( !(PIND & (1<<PORTD5))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.buttonWaveMidiControl;
			midiMsg[3] = lfo1Settings.noiseValue;
			diodesSettings.ledMuxZero &= 252;
			diodesSettings.ledMuxOne &= 63; 
			diodesSettings.ledMuxOne |= 64;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 50){
		if( !(PIND & (1<<PORTD5))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.buttonSetupsMidiControl;
			midiMsg[3] = lfo1Settings.oscOnePlusTwoValue;
			diodesSettings.ledMuxOne &= 199; 
			diodesSettings.ledMuxOne |= 32;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 51){
		if( !(PIND & (1<<PORTD5))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.buttonSetupsMidiControl;
			midiMsg[3] = lfo1Settings.filterValue;
			diodesSettings.ledMuxOne &= 199; 
			diodesSettings.ledMuxOne |= 16;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 52){
		if( !(PIND & (1<<PORTD5))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo1Settings.buttonSetupsMidiControl;
			midiMsg[3] = lfo1Settings.pwValue;
			diodesSettings.ledMuxOne &= 199; 
			diodesSettings.ledMuxOne |= 8;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 54){
			if( !(PIND & (1<<PORTD5)))
			{
				if(lfo1Settings.syncValue == 0 && lfo1Settings.syncWasPressed == 0)
				{
					midiMsg[0] = 0x09;
					midiMsg[1] = MIDI_PORT;
					midiMsg[2] = 0x7f & lfo1Settings.buttonSyncMidiControl;
					midiMsg[3] = 127;
					lfo1Settings.syncValue = 127;
					lfo1Settings.syncWasPressed = 1;
					diodesSettings.ledMuxOne |= 4;
					usbSetInterrupt(midiMsg, 4);
				}
				if(lfo1Settings.syncValue == 127 && lfo1Settings.syncWasPressed == 0)
				{
					midiMsg[0] = 0x09;
					midiMsg[1] = MIDI_PORT;
					midiMsg[2] = 0x7f & lfo1Settings.buttonSyncMidiControl;
					midiMsg[3] = 0;
					lfo1Settings.syncValue = 0;
					lfo1Settings.syncWasPressed = 1;
					diodesSettings.ledMuxOne &= 251;
					usbSetInterrupt(midiMsg, 4);
				}
			}
			else
			{
				lfo1Settings.syncWasPressed = 0;
			}
		} else if(mux_channel == 53){
			if( !(PIND & (1<<PORTD5)))
			{
				if(lfo1Settings.resetValue == 0 && lfo1Settings.resetWasPressed == 0)
				{
					midiMsg[0] = 0x09;
					midiMsg[1] = MIDI_PORT;
					midiMsg[2] = 0x7f & lfo1Settings.buttonResetMidiControl;
					midiMsg[3] = 127;
					lfo1Settings.resetValue = 127;
					lfo1Settings.resetWasPressed = 1;
					diodesSettings.ledMuxOne |= 2;
					usbSetInterrupt(midiMsg, 4);
				}
				if(lfo1Settings.resetValue == 127 && lfo1Settings.resetWasPressed == 0)
				{
					midiMsg[0] = 0x09;
					midiMsg[1] = MIDI_PORT;
					midiMsg[2] = 0x7f & lfo1Settings.buttonResetMidiControl;
					midiMsg[3] = 0;
					lfo1Settings.resetValue = 0;
					lfo1Settings.resetWasPressed = 1;
					diodesSettings.ledMuxOne &= 253;
					usbSetInterrupt(midiMsg, 4);
				}
			}
			else
			{
				lfo1Settings.resetWasPressed = 0;
			}
		} else if(mux_channel == 55){
			if( !(PIND & (1<<PORTD5))){
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo2Settings.buttonWaveMidiControl;
				midiMsg[3] = lfo2Settings.triangleValue;
				diodesSettings.ledMuxOne &= 254;
				diodesSettings.ledMuxTwo &= 31;
				diodesSettings.ledMuxOne |= 1;
				usbSetInterrupt(midiMsg, 4);
			}
	}

}

void buttonsTwoController(int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 56){
		if( !(PIND & (1<<PORTD6))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.buttonWaveMidiControl;
			midiMsg[3] = lfo2Settings.squareValue;
			diodesSettings.ledMuxOne &= 254;
			diodesSettings.ledMuxTwo &= 31;
			diodesSettings.ledMuxTwo |= 128;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 57){
		if( !(PIND & (1<<PORTD6))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.buttonWaveMidiControl;
			midiMsg[3] = lfo2Settings.sinusValue;
			diodesSettings.ledMuxOne &= 254;
			diodesSettings.ledMuxTwo &= 31;
			diodesSettings.ledMuxTwo |= 64;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 58){
		if( !(PIND & (1<<PORTD6))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.buttonWaveMidiControl;
			midiMsg[3] = lfo2Settings.noiseValue;
			diodesSettings.ledMuxOne &= 254;
			diodesSettings.ledMuxTwo &= 31;
			diodesSettings.ledMuxTwo |= 32;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 59){
		if( !(PIND & (1<<PORTD6))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.buttonSetupsMidiControl;
			midiMsg[3] = lfo2Settings.oscOneValue;
			diodesSettings.ledMuxTwo &= 227;
			diodesSettings.ledMuxTwo |= 16;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 60){
		if( !(PIND & (1<<PORTD6))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & lfo2Settings.buttonSetupsMidiControl;
			midiMsg[3] = lfo2Settings.oscMixValue;
			diodesSettings.ledMuxTwo &= 227;
			diodesSettings.ledMuxTwo |= 8;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 61){
			if( !(PIND & (1<<PORTD6)))
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo2Settings.buttonSetupsMidiControl;
				midiMsg[3] = lfo2Settings.ampValue;
				diodesSettings.ledMuxTwo &= 227;
				diodesSettings.ledMuxTwo |= 4;
				usbSetInterrupt(midiMsg, 4);
				
			}
		} else if(mux_channel == 63){
		if( !(PIND & (1<<PORTD6)))
		{
			if(lfo2Settings.syncValue == 0 && lfo2Settings.syncWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo2Settings.buttonSyncMidiControl;
				midiMsg[3] = 127;
				lfo2Settings.syncValue = 127;
				lfo2Settings.syncWasPressed = 1;
				diodesSettings.ledMuxTwo |= 2;
				usbSetInterrupt(midiMsg, 4);
			}
			if(lfo2Settings.syncValue == 127 && lfo2Settings.syncWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo2Settings.buttonSyncMidiControl;
				midiMsg[3] = 0;
				lfo2Settings.syncValue = 0;
				lfo2Settings.syncWasPressed = 1;
				diodesSettings.ledMuxTwo &= 254;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			lfo2Settings.syncWasPressed = 0;
		}
		} else if(mux_channel == 62){
		if( !(PIND & (1<<PORTD6)))
		{
			if(lfo2Settings.resetValue == 0 && lfo2Settings.resetWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo2Settings.buttonResetMidiControl;
				midiMsg[3] = 127;
				lfo2Settings.resetValue = 127;
				lfo2Settings.resetWasPressed = 1;
				diodesSettings.ledMuxTwo |= 1;
				usbSetInterrupt(midiMsg, 4);
			}
			if(lfo2Settings.resetValue == 127 && lfo2Settings.resetWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & lfo2Settings.buttonResetMidiControl;
				midiMsg[3] = 0;
				lfo2Settings.resetValue = 0;
				lfo2Settings.resetWasPressed = 1;
				diodesSettings.ledMuxTwo &= 254;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			lfo2Settings.resetWasPressed = 0;
		}
	}

}

void buttonsThreeController(int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 64){
		if( !(PIND & (1<<PORTD7)))
		{
			if(modEnvSettings.pwValue == 0 && modEnvSettings.pwWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.pwMidiControl;
				midiMsg[3] = 127;
				modEnvSettings.pwValue = 127;
				modEnvSettings.pwWasPressed = 1;
				diodesSettings.ledMuxThree |= 128;
				usbSetInterrupt(midiMsg, 4);
			}
			if(modEnvSettings.pwValue == 127 && modEnvSettings.pwWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.pwMidiControl;
				midiMsg[3] = 0;
				modEnvSettings.pwValue = 0;
				modEnvSettings.pwWasPressed = 1;
				diodesSettings.ledMuxThree &= 127;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			modEnvSettings.pwWasPressed = 0;
		}
		} else if(mux_channel == 65){
		if( !(PIND & (1<<PORTD7)))
		{
			if(modEnvSettings.lfoOneAmtValue == 0 && modEnvSettings.lfoOneAmtWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.lfoOneAmtMidiControl;
				midiMsg[3] = 127;
				modEnvSettings.lfoOneAmtValue = 127;
				modEnvSettings.lfoOneAmtWasPressed = 1;
				diodesSettings.ledMuxThree |= 64;
				usbSetInterrupt(midiMsg, 4);
			}
			if(modEnvSettings.lfoOneAmtValue == 127 && modEnvSettings.lfoOneAmtWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.lfoOneAmtMidiControl;
				midiMsg[3] = 0;
				modEnvSettings.lfoOneAmtValue = 0;
				modEnvSettings.lfoOneAmtWasPressed = 1;
				diodesSettings.ledMuxThree &= 191;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			modEnvSettings.lfoOneAmtWasPressed = 0;
		}
		} else if(mux_channel == 66){
		if( !(PIND & (1<<PORTD7)))
		{
			if(modEnvSettings.oscOneLevelValue == 0 && modEnvSettings.oscOneLevelWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.oscOneLevelMidiControl;
				midiMsg[3] = 127;
				modEnvSettings.oscOneLevelValue = 127;
				modEnvSettings.oscOneLevelWasPressed = 1;
				diodesSettings.ledMuxThree |= 32;
				usbSetInterrupt(midiMsg, 4);
			}
			if(modEnvSettings.oscOneLevelValue == 127 && modEnvSettings.oscOneLevelWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.oscOneLevelMidiControl;
				midiMsg[3] = 0;
				modEnvSettings.oscOneLevelValue = 0;
				modEnvSettings.oscOneLevelWasPressed = 1;
				diodesSettings.ledMuxThree &= 223;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			modEnvSettings.oscOneLevelWasPressed = 0;
		}
		} else if(mux_channel == 67){
		if( !(PIND & (1<<PORTD7)))
		{
			if(modEnvSettings.oscTwoPitchValue == 0 && modEnvSettings.oscTwoPitchWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.oscTwoPitchMidiControl;
				midiMsg[3] = 127;
				modEnvSettings.oscTwoPitchValue = 127;
				modEnvSettings.oscTwoPitchWasPressed = 1;
				diodesSettings.ledMuxThree |= 16;
				usbSetInterrupt(midiMsg, 4);
			}
			if(modEnvSettings.oscTwoPitchValue == 127 && modEnvSettings.oscTwoPitchWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & modEnvSettings.oscTwoPitchMidiControl;
				midiMsg[3] = 0;
				modEnvSettings.oscTwoPitchValue = 0;
				modEnvSettings.oscTwoPitchWasPressed = 1;
				diodesSettings.ledMuxThree &= 239;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			modEnvSettings.oscTwoPitchWasPressed = 0;
		}
		} else if(mux_channel == 68){
		if( !(PIND & (1<<PORTD7)))
		{
			if(outputSettings.driveValue == 0 && outputSettings.driveWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & outputSettings.driveMidiControl;
				midiMsg[3] = 127;
				outputSettings.driveValue = 127;
				outputSettings.driveWasPressed = 1;
				diodesSettings.ledMuxThree |= 8;
				usbSetInterrupt(midiMsg, 4);
			}
			if(outputSettings.driveValue == 127 && outputSettings.driveWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & outputSettings.driveMidiControl;
				midiMsg[3] = 0;
				outputSettings.driveValue = 0;
				outputSettings.driveWasPressed = 1;
				diodesSettings.ledMuxThree &= 247;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			outputSettings.driveWasPressed = 0;
		}
		} else if(mux_channel == 69){
		if( !(PIND & (1<<PORTD7)))
		{
			if(otherControlsSettings.dualValue == 0 && otherControlsSettings.dualWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & otherControlsSettings.dualMidiControl;
				midiMsg[3] = 127;
				otherControlsSettings.dualValue = 127;
				otherControlsSettings.dualWasPressed = 1;
				diodesSettings.ledMuxThree |= 4;
				usbSetInterrupt(midiMsg, 4);
			}
			if(otherControlsSettings.dualValue == 127 && otherControlsSettings.dualWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & otherControlsSettings.dualMidiControl;
				midiMsg[3] = 0;
				otherControlsSettings.dualValue = 0;
				otherControlsSettings.dualWasPressed = 1;
				diodesSettings.ledMuxThree &= 251;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			otherControlsSettings.dualWasPressed = 0;
		}
		} else if(mux_channel == 70){
		if( !(PIND & (1<<PORTD7)))
		{
			if(otherControlsSettings.analogValue == 0 && otherControlsSettings.analogWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & otherControlsSettings.analogMidiControl;
				midiMsg[3] = 127;
				otherControlsSettings.analogValue = 127;
				otherControlsSettings.analogWasPressed = 1;
				diodesSettings.ledMuxThree |= 2;
				usbSetInterrupt(midiMsg, 4);
			}
			if(otherControlsSettings.analogValue == 127 && otherControlsSettings.analogWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & otherControlsSettings.analogMidiControl;
				midiMsg[3] = 0;
				otherControlsSettings.analogValue = 0;
				otherControlsSettings.analogWasPressed = 1;
				diodesSettings.ledMuxThree &= 253;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			otherControlsSettings.analogWasPressed = 0;
		}
		} else if(mux_channel == 71){
		if( !(PIND & (1<<PORTD7)))
		{
			if(otherControlsSettings.wNoiseValue == 0 && otherControlsSettings.wNoiseWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & otherControlsSettings.wNoiseMidiControl;
				midiMsg[3] = 127;
				otherControlsSettings.wNoiseValue = 127;
				otherControlsSettings.wNoiseWasPressed = 1;
				diodesSettings.ledMuxThree |= 1;
				usbSetInterrupt(midiMsg, 4);
			}
			if(otherControlsSettings.wNoiseValue == 127 && otherControlsSettings.wNoiseWasPressed == 0)
			{
				midiMsg[0] = 0x09;
				midiMsg[1] = MIDI_PORT;
				midiMsg[2] = 0x7f & otherControlsSettings.wNoiseMidiControl;
				midiMsg[3] = 0;
				otherControlsSettings.wNoiseValue = 0;
				otherControlsSettings.wNoiseWasPressed = 1;
				diodesSettings.ledMuxThree &= 254;
				usbSetInterrupt(midiMsg, 4);
			}
		}
		else
		{
			otherControlsSettings.wNoiseWasPressed = 0;
		}
	}

}

void buttonsFourController(int mux_channel){
	uchar midiMsg[4];
		if(mux_channel == 73){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc1Settings.buttonWaveMidiControl;
			midiMsg[3] = osc1Settings.triangleValue;
			diodesSettings.ledMuxFour &= 135;
			diodesSettings.ledMuxFour |= 64;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 74){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc1Settings.buttonWaveMidiControl;
			midiMsg[3] = osc1Settings.squareValue;
			diodesSettings.ledMuxFour &= 135;
			diodesSettings.ledMuxFour |= 32;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 75){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc1Settings.buttonWaveMidiControl;
			midiMsg[3] = osc1Settings.sinusValue;
			diodesSettings.ledMuxFour &= 135;
			diodesSettings.ledMuxFour |= 16;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 76){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc1Settings.buttonWaveMidiControl;
			midiMsg[3] = osc1Settings.noiseValue;
			diodesSettings.ledMuxFour &= 135;
			diodesSettings.ledMuxFour |= 8;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 77){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc2Settings.buttonWaveMidiControl;
			midiMsg[3] = osc2Settings.triangleValue;
			diodesSettings.ledMuxFour &= 248;
			diodesSettings.ledMuxFive &= 127;
			diodesSettings.ledMuxFour |= 4;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 78){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc2Settings.buttonWaveMidiControl;
			midiMsg[3] = osc2Settings.squareValue;
			diodesSettings.ledMuxFour &= 248;
			diodesSettings.ledMuxFive &= 127;			
			diodesSettings.ledMuxFour |= 2;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 79){
		if( !(PINB & (1<<PORTB0))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc2Settings.buttonWaveMidiControl;
			midiMsg[3] = osc2Settings.sinusValue;
			diodesSettings.ledMuxFour &= 248;
			diodesSettings.ledMuxFive &= 127;
			diodesSettings.ledMuxFour |= 1;
			usbSetInterrupt(midiMsg, 4);
		}
	}

}

void buttonsFiveController(int mux_channel){
	uchar midiMsg[4];
	if(mux_channel == 80){
		if( !(PINB & (1<<PORTB4))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc2Settings.buttonWaveMidiControl;
			midiMsg[3] = osc2Settings.noiseValue;
			diodesSettings.ledMuxFour &= 248;
			diodesSettings.ledMuxFive &= 127;
			diodesSettings.ledMuxFive |= 128;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 81){
		if( !(PINB & (1<<PORTB4))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc3Settings.buttonWaveMidiControl;
			midiMsg[3] = osc3Settings.triangleValue;
			diodesSettings.ledMuxFive &= 159;
			diodesSettings.ledMuxFive |= 64;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 82){
		if( !(PINB & (1<<PORTB4))){
			midiMsg[0] = 0x09;
			midiMsg[1] = MIDI_PORT;
			midiMsg[2] = 0x7f & osc3Settings.buttonWaveMidiControl;
			midiMsg[3] = osc3Settings.squareValue;
			diodesSettings.ledMuxFive &= 159;
			diodesSettings.ledMuxFive |= 32;
			usbSetInterrupt(midiMsg, 4);
		}
		} else if(mux_channel == 83){
				if( !(PINB & (1<<PORTB4)))
				{
					if(oscMixSettings.ringmodValue == 0 && oscMixSettings.ringmodWasPressed == 0)
					{
						midiMsg[0] = 0x09;
						midiMsg[1] = MIDI_PORT;
						midiMsg[2] = 0x7f & oscMixSettings.ringmodMidiControl;
						midiMsg[3] = 127;
						oscMixSettings.ringmodValue = 127;
						oscMixSettings.ringmodWasPressed = 1;
						diodesSettings.ledMuxFive |= 16;
						usbSetInterrupt(midiMsg, 4);
					}
					if(oscMixSettings.ringmodValue == 127 && oscMixSettings.ringmodWasPressed == 0)
					{
						midiMsg[0] = 0x09;
						midiMsg[1] = MIDI_PORT;
						midiMsg[2] = 0x7f & oscMixSettings.ringmodMidiControl;
						midiMsg[3] = 0;
						oscMixSettings.ringmodValue = 0;
						oscMixSettings.ringmodWasPressed = 1;
						diodesSettings.ledMuxFive &= 239;
						usbSetInterrupt(midiMsg, 4);
					}
				}
				else
				{
					oscMixSettings.ringmodWasPressed = 0;
				}
		} 

}

void atmegaV1()
{
		_delay_ms(500);
		int mux_channel=0;
		int counter = 0;
		DDRB |= (1<<DDB1);
		DDRB |= (1<<DDB2);
		DDRB |= (1<<DDB3);

		DDRD &= ~(1<<DDD4);
		PORTD |= (1<<PORTD4);
		DDRD &= ~(1<<DDD5);
		PORTD |= (1<<PORTD5);
		DDRD &= ~(1<<DDD6);
		PORTD |= (1<<PORTD6);
		DDRD &= ~(1<<DDD7);
		PORTD |= (1<<PORTD7);

		DDRB &= ~(1<<DDB0);
		PORTB |= (1<<PORTB0);
		DDRB &= ~(1<<DDB4);
		PORTB |= (1<<PORTB4);
		
		wdt_enable(WDTO_1S);
		hardwareInit();
		odDebugInit();
		usbInit();
		sei();
		UART_init(9600,true,true);
		for (;;) {
			wdt_reset();
			usbPoll();
			switch(mux_channel)
			{
				case 0:
				case 8:
				case 16:
				case 24:
				case 32:
				case 40:
				case 48:
				case 56:
				case 64:
				case 72:
				case 80:
				PORTB &= ~(1<<PORTB1);
				PORTB &= ~(1<<PORTB2);
				PORTB &= ~(1<<PORTB3);
				break;
				
				case 1:
				case 9:
				case 17:
				case 25:
				case 33:
				case 41:
				case 49:
				case 57:
				case 65:
				case 73:
				case 81:
				PORTB &= ~(1<<PORTB1);
				PORTB &= ~(1<<PORTB2);
				PORTB |= (1<<PORTB3);
				break;
				
				case 2:
				case 10:
				case 18:
				case 26:
				case 34:
				case 42:
				case 50:
				case 58:
				case 66:
				case 74:
				case 82:
				PORTB &= ~(1<<PORTB1);
				PORTB |= (1<<PORTB2);
				PORTB &= ~(1<<PORTB3);
				break;
				
				case 3:
				case 11:
				case 19:
				case 27:
				case 35:
				case 43:
				case 51:
				case 59:
				case 67:
				case 75:
				case 83:
				PORTB &= ~(1<<PORTB1);
				PORTB |= (1<<PORTB2);
				PORTB |= (1<<PORTB3);
				break;
				
				case 4:
				case 12:
				case 20:
				case 28:
				case 36:
				case 44:
				case 52:
				case 60:
				case 68:
				case 76:
				case 84:
				PORTB |= (1<<PORTB1);
				PORTB &= ~(1<<PORTB2);
				PORTB &= ~(1<<PORTB3);
				break;
				
				case 5:
				case 13:
				case 21:
				case 29:
				case 37:
				case 45:
				case 53:
				case 61:
				case 69:
				case 77:
				case 85:
				PORTB |= (1<<PORTB1);
				PORTB &= ~(1<<PORTB2);
				PORTB |= (1<<PORTB3);
				break;
				
				case 6:
				case 14:
				case 22:
				case 30:
				case 38:
				case 46:
				case 54:
				case 62:
				case 70:
				case 78:
				case 86:
				PORTB |= (1<<PORTB1);
				PORTB |= (1<<PORTB2);
				PORTB &= ~(1<<PORTB3);
				break;
				
				case 7:
				case 15:
				case 23:
				case 31:
				case 39:
				case 47:
				case 55:
				case 63:
				case 71:
				case 79:
				case 87:
				PORTB |= (1<<PORTB1);
				PORTB |= (1<<PORTB2);
				PORTB |= (1<<PORTB3);
				break;

				default:
				break;
			}
			if (usbInterruptIsReady()) {
				int value;
				if(mux_channel<8){
				value = adc(0);	
				value = (value - 1023) * (-1);
				adcZeroController(value, mux_channel);
				} else if(mux_channel >= 8 && mux_channel<16){
				value = adc(1);
				value = (value - 1023) * (-1);
				adcOneController(value, mux_channel);
				} else if(mux_channel >= 16 && mux_channel<24){
				value = adc(2);
				value = (value - 1023) * (-1);
				adcTwoController(value, mux_channel);
				} else if(mux_channel >= 24 && mux_channel<32){
				value = adc(3);
				value = (value - 1023) * (-1);
				adcThreeController(value, mux_channel);
				} else if(mux_channel >= 32 && mux_channel<35){
				value = adc(4);
				value = (value - 1023) * (-1);
				adcFourController(value, mux_channel);
				} 
				 else if(mux_channel >= 40 && mux_channel<48){
					buttonsZeroController(mux_channel);
				}else if(mux_channel >= 48 && mux_channel<56){
					 buttonsOneController(mux_channel);
				} else if(mux_channel >= 56 && mux_channel<64){
					 buttonsTwoController(mux_channel);
				} else if(mux_channel >= 64 && mux_channel<72){
					buttonsThreeController(mux_channel);
				}else if(mux_channel >= 72 && mux_channel<80){
					buttonsFourController(mux_channel);
				} else if(mux_channel >= 80 && mux_channel<88){
					buttonsFiveController(mux_channel);
				}
				mux_channel++;
				if(mux_channel >=88) mux_channel = 0;
			}
			
			if(counter == 10000){
				USART_send(oledV1Settings.atkLine);
				USART_send(oledV1Settings.decLine);
				USART_send(oledV1Settings.susLine);
				USART_send(oledV1Settings.relLine);
				USART_send(oledV2Settings.atkLine);
				USART_send(oledV2Settings.decLine);
				USART_send(oledV2Settings.susLine);
				USART_send(oledV2Settings.relLine);
				counter=0;
			}
			counter++;
		}
}

void atmegaV2()
{
	DDRB |= (1<<DDB1);
	DDRB |= (1<<DDB2);
	DDRB |= (1<<DDB3);

	
	sei();
	UART_init(9600,true,true);
	
	uint8_t x;
	while(UART_available()){
		x = UART_read();
	}
	for (;;) {
		uint8_t oledBuffer[8] = {0};
			if(UART_available()){
				oledBuffer[0] = UART_read();
				oledBuffer[1] = UART_read();
				oledBuffer[2] = UART_read();
				oledBuffer[3] = UART_read();
				oledBuffer[4] = UART_read();
				oledBuffer[5] = UART_read();
				oledBuffer[6] = UART_read();
				oledBuffer[7] = UART_read();
				if(oledBuffer[0] != oledV1Settings.atkLine || oledBuffer[1] != oledV1Settings.decLine || oledBuffer[2] != oledV1Settings.susLine || oledBuffer[3] != oledV1Settings.relLine)
				{
					PORTB &= ~(1<<PORTB1);
					PORTB &= ~(1<<PORTB2);
					PORTB &= ~(1<<PORTB3);
					oledV1Settings.atkLine = oledBuffer[0];
					oledV1Settings.decLine = oledBuffer[1];
					oledV1Settings.susLine = oledBuffer[2];
					oledV1Settings.relLine = oledBuffer[3];
					OLED_Init();
					OLED_DrawLine(0,63,oledV1Settings.atkLine,0);
					OLED_DrawLine(oledV1Settings.atkLine,0,(oledV1Settings.atkLine + oledV1Settings.decLine),(63-oledV1Settings.susLine));
					OLED_DrawLine((oledV1Settings.atkLine + oledV1Settings.decLine),(63-oledV1Settings.susLine),(oledV1Settings.atkLine + oledV1Settings.decLine + oledV1Settings.relLine),63);
					OLED_ExecuteLines();	
				}
				if(oledBuffer[4] != oledV2Settings.atkLine || oledBuffer[5] != oledV2Settings.decLine || oledBuffer[6] != oledV2Settings.susLine || oledBuffer[7] != oledV2Settings.relLine)
				{
					PORTB &= ~(1<<PORTB1);
					PORTB &= ~(1<<PORTB2);
					PORTB |= (1<<PORTB3);
					oledV2Settings.atkLine = oledBuffer[4];
					oledV2Settings.decLine = oledBuffer[5];
					oledV2Settings.susLine = oledBuffer[6];
					oledV2Settings.relLine = oledBuffer[7];
					OLED_Init();
					OLED_DrawLine(0,63,oledV2Settings.atkLine,0);
					OLED_DrawLine(oledV2Settings.atkLine,0,(oledV2Settings.atkLine + oledV2Settings.decLine),(63-oledV2Settings.susLine));
					OLED_DrawLine((oledV2Settings.atkLine + oledV2Settings.decLine),(63-oledV2Settings.susLine),(oledV2Settings.atkLine + oledV2Settings.decLine + oledV2Settings.relLine),63);
					OLED_ExecuteLines();
				}
				while(UART_available()){
					x = UART_read();
				}
			}
			

	}
}

int main(void)
{
	//atmegaV1();
	atmegaV2();
	return 0;
}

