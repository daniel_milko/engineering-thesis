/*
 * mod_env.h
 *
 * Created: 26.11.2020 14:36:14
 *  Author: danie
 */ 


#ifndef MOD_ENV_H_
#define MOD_ENV_H_

typedef struct  {
	
	int atkValue;
	int decValue;
	int plusAmtMinusValue;

	int pwValue;
	int lfoOneAmtValue;
	int oscOneLevelValue;
	int oscTwoPitchValue;

	int pwWasPressed;
	int lfoOneAmtWasPressed;
	int oscOneLevelWasPressed;
	int oscTwoPitchWasPressed;

	int atkMidiControl;
	int decMidiControl;
	int plusAmtMinusMidiControl;
	int pwMidiControl;
	int lfoOneAmtMidiControl;
	int oscOneLevelMidiControl;
	int oscTwoPitchMidiControl;
	
} ModEnvSettings;

ModEnvSettings modEnvSettings = {
	.atkValue = -1,
	.decValue = -1,
	.plusAmtMinusValue = -1,

	.pwValue = 0,
	.lfoOneAmtValue = 0,
	.oscOneLevelValue = 0,
	.oscTwoPitchValue = 0,

	.pwWasPressed = 0,
	.lfoOneAmtWasPressed = 0,
	.oscOneLevelWasPressed = 0,
	.oscTwoPitchWasPressed = 0,
	
	.atkMidiControl = 20,
	.decMidiControl = 21,
	.plusAmtMinusMidiControl = 22,
	.pwMidiControl = 23,
	.lfoOneAmtMidiControl = 24,
	.oscOneLevelMidiControl = 25,
	.oscTwoPitchMidiControl = 26,
};



#endif /* MOD_ENV_H_ */