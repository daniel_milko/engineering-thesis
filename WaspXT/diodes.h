/*
 * diodes.h
 *
 * Created: 17.12.2020 14:42:22
 *  Author: danie
 */ 


#ifndef DIODES_H_
#define DIODES_H_

typedef struct  {
	
	uint8_t ledMuxZero;
	uint8_t ledMuxOne;
	uint8_t ledMuxTwo;
	uint8_t ledMuxThree;
	uint8_t ledMuxFour;
	uint8_t ledMuxFive;
	
} DiodesSettings;

DiodesSettings diodesSettings = {
	.ledMuxZero = 130,
	.ledMuxOne = 160,
	.ledMuxTwo = 80,
	.ledMuxThree = 2,
	.ledMuxFour = 68,
	.ledMuxFive = 64,
};




#endif /* DIODES_H_ */