/*
 * ampenv.h
 *
 * Created: 26.11.2020 15:34:39
 *  Author: danie
 */ 


#ifndef AMPENV_H_
#define AMPENV_H_


typedef struct  {
	
	int atkValue;
	int decValue;
	int susValue;
	int relValue;
	
	int atkMidiControl;
	int decMidiControl;
	int susMidiControl;
	int relMidiControl;
	
} AmpEnvSettings;

AmpEnvSettings ampEnvSettings = {
	.atkValue = -1,
	.decValue = -1,
	.susValue = -1,
	.relValue = -1,
	
	.atkMidiControl = 36,
	.decMidiControl = 37,
	.susMidiControl = 38,
	.relMidiControl = 39,
};



#endif /* AMPENV_H_ */