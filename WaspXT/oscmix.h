/*
 * oscmix.h
 *
 * Created: 26.11.2020 16:10:23
 *  Author: danie
 */ 


#ifndef OSCMIX_H_
#define OSCMIX_H_


typedef struct  {
	
	int oscmixValue;
	int pwValue;
	int fmValue;
	
	int ringmodValue;
	int ringmodWasPressed;

	
	int oscmixMidiControl;
	int pwMidiControl;
	int fmMidiControl;
	int ringmodMidiControl;
	
} OscMixSettings;

OscMixSettings oscMixSettings = {
	.oscmixValue = -1,
	.pwValue = -1,
	.fmValue = -1,

	.ringmodValue = 0,
	.ringmodWasPressed = 0,
	
	.oscmixMidiControl = 53,
	.pwMidiControl = 54,
	.fmMidiControl = 55,
	.ringmodMidiControl = 56,
};


#endif /* OSCMIX_H_ */