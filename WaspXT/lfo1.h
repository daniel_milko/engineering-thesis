/*
 * lfo1.h
 *
 * Created: 26.11.2020 13:39:21
 *  Author: danie
 */ 


#ifndef LFO1_H_
#define LFO1_H_


typedef struct  {
	
	int amountValue;
	int rateValue;
	int delayValue;
	
	int triangleValue;
	int squareValue;
	int sinusValue;
	int noiseValue;
	
	int oscOnePlusTwoValue;
	int filterValue;
	int pwValue;
	
	int syncValue;
	int resetValue;
	int syncWasPressed;
	int resetWasPressed;
	
	int amountMidiControl;
	int rateMidiControl;
	int delayMidiControl;
	int buttonWaveMidiControl;
	int buttonSetupsMidiControl;
	int buttonSyncMidiControl;
	int buttonResetMidiControl;
	
} Lfo1Settings;

Lfo1Settings lfo1Settings = {
	.amountValue = -1,
	.rateValue = -1,
	.delayValue = -1,
	
	.triangleValue = 0,
	.squareValue = 50,
	.sinusValue = 100,
	.noiseValue = 127,
	
	.oscOnePlusTwoValue = 0,
	.filterValue = 70,
	.pwValue = 127,
	
	.resetValue = 0,
	.resetWasPressed = 0,
	.syncValue = 0,
	.syncWasPressed = 0,
	
	.amountMidiControl = 6,
	.rateMidiControl = 7,
	.delayMidiControl = 8,
	.buttonWaveMidiControl = 9,
	.buttonSetupsMidiControl = 10,
	.buttonSyncMidiControl = 11,
	.buttonResetMidiControl = 12,
};


#endif /* LFO1_H_ */