/*
 * velocity.h
 *
 * Created: 26.11.2020 15:01:35
 *  Author: danie
 */ 


#ifndef VELOCITY_H_
#define VELOCITY_H_

typedef struct  {
	
	int filterValue;
	int ampValue;
	
	int filterMidiControl;
	int ampMidiControl;
	
} VelocitySettings;

VelocitySettings velocitySettings = {
	.filterValue = -1,
	.ampValue = -1,
	
	.filterMidiControl = 27,
	.ampMidiControl = 28,
};




#endif /* VELOCITY_H_ */